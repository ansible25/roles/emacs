---
# tasks file for emacs
- name: Check for existing emacs binary
  ansible.builtin.stat:
    path: /usr/local/bin/emacs
  register: emacs

- name: Check for existing emacs systemd service
#  become: true
  ansible.builtin.stat:
    path: ~/.config/systemd/user/emacs.service
  register: emacs_service

- name: Check for existing emacs config directory
#  become: true
  ansible.builtin.stat:
    path: ~/.emacs.d
  register: emacs_config

#- name: Bail if emacs and emacs service are already present
#  meta: end_play
#  when: emacs.stat.exists and emacs_service.stat.exists

- name: Get Emacs source tarball
  get_url:
    url: https://ftp.gnu.org/pub/gnu/emacs/emacs-{{ emacs_version }}.tar.xz
    dest: /tmp/emacs.tar.xz
  when: not emacs.stat.exists and not emacs_service.stat.exists

- name: Get Emacs .sig file
  get_url:
    url: https://ftp.gnu.org/pub/gnu/emacs/emacs-{{ emacs_version }}.tar.xz.sig
    dest: /tmp/emacs.tar.xz.sig
  when: not emacs.stat.exists and not emacs_service.stat.exists

- name: Get GNU signing key
  get_url:
    url: https://ftp.gnu.org/gnu/gnu-keyring.gpg
    dest: /tmp/gnu-keyring.gpg
  when: not emacs.stat.exists and not emacs_service.stat.exists

- name: Verify Emacs tarball
  command:
    chdir: /tmp
    cmd: gpg --verify --keyring ./gnu-keyring.gpg emacs.tar.xz.sig
  changed_when: False
  when: not emacs.stat.exists and not emacs_service.stat.exists

- name: Extract Emacs tarball
  unarchive:
    src: /tmp/emacs.tar.xz
    dest: /tmp/
    remote_src: yes
    creates: /tmp/emacs-{{ emacs_version }}
  when: not emacs.stat.exists and not emacs_service.stat.exists

- name: Download make utils
  become: yes
  package:
    name: "{{ item }}"
    state: present
  loop:
    - make
    - gcc
    - autoconf

# so that checkinstall is available
- name: Add backports if Debian 10
  become: true
  ansible.builtin.apt_repository:
    repo: deb http://deb.debian.org/debian buster-backports main
    state: present
  when: ansible_distribution == 'Debian' and
        ansible_distribution_major_version == '10'

- name: Update apt-cache if Debian 10
  become: true
  apt:
    update_cache: true
    cache_valid_time: 0
  when: ansible_distribution == 'Debian' and
        ansible_distribution_major_version == '10'

- name: Download dependencies
  become: yes
  package:
    name: "{{ item }}"
    state: present
  loop:
    - gnutls-devel
    - ncurses-devel
    - libXaw-devel
    - libjpeg-turbo-devel
    - libpng-devel
    - giflib-devel
    - libtiff-devel
    - librsvg2-devel
    - gtk3-devel
    - libotf-devel
    - libXft-devel
    - libxml2-devel
    - GConf2-devel
    - dbus-devel
    - gsettings-desktop-schemas-devel
    - systemd-devel
  when: ansible_facts['os_family'] == "RedHat"

- name: Download dependencies (PureOS/Debian)
  become: yes
  package:
    name: "{{ item }}"
    state: present
  loop:
    - build-essential
    - xserver-xorg-dev
    - libxaw7-dev
    - xaw3dg-dev
    - libjpeg-dev
    - libgif-dev
    - libtiff-dev
    - libgnutls28-dev
    - libtinfo-dev
    - libxrandr-dev
    - libxext-dev
    - libsystemd-dev
    - libgconf2-dev
    - libx11-xcb-dev
    - libfontconfig-dev
    - libxpm-dev
    - liblcms2-dev
    - libxinerama-dev
    - libjansson-dev
    - libxft-dev
    - libxml++2.6-dev
    - libgtk-3-dev
    - texinfo
    - checkinstall
  when: ansible_facts['distribution_release'] == "pureos" or
        ansible_distribution == 'Debian'

- name: Download dependencies (Ubuntu)
  become: yes
  package:
    name: "{{ item }}"
    state: present
  loop:
    - libtiff-dev
    - build-essential
    - libgtk-3-dev
    - libtiff5-dev
    - libgif-dev
    - libjpeg-dev
    - libpng-dev
    - libxpm-dev
    - libncurses-dev
    - libgnutls28-dev
    - texinfo
    - checkinstall
  when: ansible_distribution == 'Ubuntu'

- name: Gather the package facts on Debian
  ansible.builtin.package_facts:
    manager: auto
  when: ansible_facts['distribution_release'] == "pureos" or
        (ansible_distribution == 'Debian' and
        ansible_distribution_major_version == '11')

- name: Install gtk+3 software
  become: true
  command:
    cmd: apt-get install -y gtk+3 # noqa 303
  when: ansible_facts['distribution_release'] == "pureos" or
        (ansible_distribution == 'Debian' and
        ansible_distribution_major_version == '11')
  changed_when: false # Lazy bypass molecule idempotence

- name: Configure Emacs source
  command:
    chdir: /tmp/emacs-{{ emacs_version }}
    cmd: ./configure --with-mailutils
    creates: /tmp/emacs-{{ emacs_version }}/config.status
  when: not emacs.stat.exists and not emacs_service.stat.exists

- name: Make
  shell:
    chdir: /tmp/emacs-{{ emacs_version }}
    cmd: make && touch make.done
    creates: /tmp/emacs-{{ emacs_version }}/make.done
  when: not emacs.stat.exists and not emacs_service.stat.exists

- name: Make Install (CentOS)
  become: true
  command:
    chdir: /tmp/emacs-{{ emacs_version }}
    cmd: make install
    creates: /usr/local/bin/emacs
  when: ansible_facts['os_family'] == "RedHat" and
        not emacs.stat.exists and not emacs_service.stat.exists

- name: Make Install (PureOS/Debian)
  become: true
  command:
    chdir: /tmp/emacs-{{ emacs_version }}
    cmd: checkinstall -y --fstrans=no
    creates: /usr/local/bin/emacs
  when: (ansible_facts['distribution_release'] == "pureos" or
        ansible_facts['os_family'] == 'Debian') and
        not emacs.stat.exists and not emacs_service.stat.exists

- name: Download Emacs config repo
  ansible.builtin.git:
    repo: https://gitlab.com/emacs3/emacs.d.git
    dest: ~/.emacs.d
    version: "{{ emacs_config_repo_branch }}"
  when: not emacs_config.stat.exists

- name: Ensure user's .config directory exists
  file:
    path: ~/.config
    state: directory
    mode: 0700
  when: ansible_facts['distribution_release'] == "pureos" or
        ansible_facts['os_family'] == "Debian"

- name: Ensure user's systemd directory exists
  file:
    path: ~/.config/systemd/user
    state: directory
    mode: 0755
  when: ansible_facts['distribution_release'] == "pureos" or
        ansible_facts['os_family'] == "Debian"

- name: Copy over Emacs systemd file (CentOS)
  become: true
  copy:
    src: emacs-centos.service
    dest: /usr/lib/systemd/system/emacs@.service
    owner: root
    group: root
    mode: '0644'
  when: ansible_facts['os_family'] == "RedHat"

- name: Copy over Emacs systemd file (PureOS/Debian)
  copy:
    src: emacs.service
    dest: ~/.config/systemd/user/emacs.service
    owner: "{{ ansible_user }}"
    group: "{{ ansible_user }}"
    mode: '0644'
  when: ansible_facts['distribution_release'] == "pureos" or
        ansible_facts['os_family'] == "Debian"

- name: Execute systemctl daemon-reload (CentOS)
  become: true
  command:
    cmd: systemctl daemon-reload
  changed_when: false
  when: ansible_facts['os_family'] == "RedHat"

- name: Enable Emacs server service (CentOS)
  become: true
  systemd:
    name: "emacs@{{ ansible_user }}"
    scope: system
    state: started
    enabled: true
    #daemon_reload: true
  delay: 15
  retries: 3
  when: ansible_facts['os_family'] == "RedHat"

- name: Enable Emacs server service (PureOS/Debian)
  systemd:
    name: emacs
    scope: user
    state: started
    enabled: true
  when: ansible_facts['distribution_release'] == "pureos" or
        ansible_facts['os_family'] == "Debian"
  delay: 30
  retries: 3
