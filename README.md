Emacs
=========

Role that installs Emacs

Requirements
------------

None

Role Variables
--------------

- emacs_version: Version of Emacs install. Used in URL where sources are retrieved from.

Dependencies
------------

None

Example Playbook
----------------

 Install from Source

    - hosts: servers
      roles:
         - role: emacs

License
-------

MIT

Author Information
------------------

This role was created by [bradthebuilder](https://bradthebuilder.me)
